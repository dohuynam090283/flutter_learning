import 'models.dart';

class ExploreData {
  final List<ExploreRecipe> todayRecipes;
  final List<Post> friendPosts;
  final List<SimpleRecipe> recipes;

  ExploreData(this.todayRecipes, this.friendPosts, this.recipes);
}
